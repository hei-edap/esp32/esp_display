/* SPI Master example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "soc/gpio_struct.h"
#include "driver/gpio.h"
#include "math.h"

#define PIN_NUM_MISO 25
#define PIN_NUM_MOSI 21
#define PIN_NUM_CLK  19
#define PIN_NUM_CS   22

#define PIN_NUM_DC   23
#define PIN_NUM_RST  18
#define PIN_NUM_BCKL 5

#define INIT_ONLY 0

#define BLUE (0b11111 << 0)
#define GREEN (0b111111 << 5)
#define RED (0b11111 << 11)
#define BLACK 0
#define WHITE 0XFFFF

uint16_t color = BLACK;

spi_device_handle_t spi8 = NULL;

spi_bus_config_t buscfg = {
        .miso_io_num=PIN_NUM_MISO,
        .mosi_io_num=PIN_NUM_MOSI,
        .sclk_io_num=PIN_NUM_CLK,
        .max_transfer_sz = 240 * 240 * 18,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1
};

spi_device_interface_config_t devcfg8bits = {
        .address_bits=0,
        .clock_speed_hz=SPI_MASTER_FREQ_80M,               //Clock out at 10 MHz
        .flags = SPI_DEVICE_HALFDUPLEX,
        .mode=0,                                //SPI mode 0
        .spics_io_num=PIN_NUM_CS,               //CS pin
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
        //.pre_cb=ili_spi_pre_transfer_callback,  //Specify pre-transfer callback to handle D/C line
};

/*
 Command and arguments to begin with during screen init
*/
typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} st7789v_init;

static const st7789v_init st7789v_init_commands[] = {
        {0x11, {0},                                                                                  0x80}, // Sleep out
        {0x36, {0},                                                                                  1}, // MADCTL
        {0x3A, {0x05},                                                                               1}, // Maybe 0x55
        {0xB2, {0x0C, 0x0C, 0x00, 0x33, 0x33},                                                       5}, // Porch settings
        {0xB7, {0x75},                                                                               1}, // Gate control
        {0xBB, {0x3D},                                                                               1}, // VCOM setting
        {0xC2, {0x01},                                                                               1}, // VDV and VRH
        {0xC3, {0x19},                                                                               1}, // VRH
        {0xC4, {0x20},                                                                               1}, // VDV
        {0xC6, {0x0F},                                                                               1}, // Frame rate 60Hz, no inversion
        {0xD0, {0xA4, 0xA1},                                                                         2}, // Power control
        {0xD6, {0xA1},                                                                               1},
//    {0xB1, {0x00, 0x1B}, 2},
//    {0xF2, {0x08}, 1},
//    {0x26, {0x01}, 1},
        {0xE0, {0x70, 0x04, 0x08, 0x09, 0x09, 0x05, 0x2A, 0X33, 0x41, 0x07, 0x13, 0x13, 0x29, 0x2F}, 14}, // Gamma control
        {0XE1, {0x70, 0x03, 0x09, 0x0A, 0x09, 0x06, 0x2B, 0x34, 0x41, 0x07, 0x12, 0x14, 0x28, 0x2E}, 14}, // Gamma control
        {0x21, {0},                                                                                  0x80},
        {0x29, {0},                                                                                  0x80}, // Display on
        {0x2A, {0,    0,    0,    0xEF},                                                             4},
        {0x2B, {0,    0,    0,    0xEF},                                                             4},
        {0x2C, {0},                                                                                  0x80},
        {0,    {0},                                                                                  0xff},
};

static const int RESX = 240;
static const int RESY = 240;

//Send a command to the ILI9341. Uses spi_device_transmit, which waits until the transfer is complete.
void send_cmd(const uint8_t cmd) {
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = 9;                     //Command is 9 bits

    uint8_t data[2] = {cmd >> 1, (cmd & 1) << 7};
    //printf("Command sent : %#01x, %#01x, cmd content %#01x \n", data[0], data[1], cmd);
    t.tx_buffer = &data;               //The data is the cmd itself
    ret = spi_device_transmit(spi8, &t);  //Transmit!
    assert(ret == ESP_OK);            //Should have had no issues.
}

//Send data to the ILI9341. Uses spi_device_transmit, which waits until the transfer is complete.
void send_data(const uint8_t *data, int len) {
    esp_err_t ret;
    spi_transaction_t t;
    if (len == 0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length = 9;                 //Len is in bytes, transaction length is in bits.

    // Allocate memory for adding the 9th bit
    for (int i = 0; i < len; i++) {
        uint8_t xt[2] = {(1 << 7) | (*(data + i) >> 1), (*(data + i) & 1) << 7};
        //printf("Data sent : %#01x, %#01x, data content %#01x \n", xt[0], xt[1], data[i]);
        t.tx_buffer = &xt;               //Data
        ret = spi_device_transmit(spi8, &t);  //Transmit!
        assert(ret == ESP_OK);            //Should have had no issues.
    }
}

//Initialize the display
void screen_init() {
    int cmd = 0;
    //Initialize non-SPI GPIOs
    gpio_set_direction(PIN_NUM_DC, GPIO_MODE_OUTPUT);
    gpio_set_direction(PIN_NUM_RST, GPIO_MODE_OUTPUT);
    gpio_set_direction(PIN_NUM_BCKL, GPIO_MODE_OUTPUT);

    //Reset the display
    gpio_set_level(PIN_NUM_RST, 0);
    vTaskDelay(100 / portTICK_RATE_MS);
    gpio_set_level(PIN_NUM_RST, 1);
    vTaskDelay(100 / portTICK_RATE_MS);

    //Send all the commands
    while (st7789v_init_commands[cmd].databytes != 0xff) {
        send_cmd(st7789v_init_commands[cmd].cmd);
        send_data(st7789v_init_commands[cmd].data, st7789v_init_commands[cmd].databytes & 0x1F);
        if (st7789v_init_commands[cmd].databytes & 0x80) {
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        cmd++;
    }

    ///Enable backlight
    gpio_set_level(PIN_NUM_BCKL, 0);
}

uint16_t *frameBuffer;

void start_frame() {
    esp_err_t ret;
    int x;

    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans;

    uint16_t startcol = 0, endcol = 239;

    uint8_t data[4];
    /*Column addresses*/
    send_cmd(0x2A);
    data[0] = (startcol >> 8) & 0xFF;
    data[1] = startcol & 0xFF;
    data[2] = (endcol >> 8) & 0xFF;
    data[3] = endcol & 0xFF;
    send_data(data, 4);

    /*Page addresses*/
    uint16_t startrow = 40, endrow = 279;
    send_cmd(0x2B);
    data[0] = (startrow >> 8) & 0xFF;
    data[1] = startrow & 0xFF;
    data[2] = (endrow >> 8) & 0xFF;
    data[3] = endrow & 0xFF;
    send_data(data, 4);

    /*Memory write*/
    send_cmd(0x2C);
}

void send_frame() {
    esp_err_t ret;

    //Transaction descriptors. Declared static so they're not allocated on the stack; we need this memory even when this
    //function is finished because the SPI driver needs access to it even while we're already calculating the next line.
    static spi_transaction_t trans;

    memset(&trans, 0, sizeof(spi_transaction_t));
    trans.flags = 0;
    trans.length = 240 * 18;

    // Buffer for a single line of 240 pixels, each having 18 bits (16 bits colors + 2 bits DC / pixel)
    uint8_t *buffer = malloc((240 * 18) / 8);
    assert(buffer != NULL);
    trans.tx_buffer = buffer;

    for (int line = 0; line < 240; line++) {
        // For each column, prepare
        uint16_t lineOffset = line * 240;

        for (int i = 0; i < 240 / 4; i++) {
            uint16_t p0 = frameBuffer[0 + i * 4 + lineOffset];
            uint16_t p1 = frameBuffer[1 + i * 4 + lineOffset];
            uint16_t p2 = frameBuffer[2 + i * 4 + lineOffset];
            uint16_t p3 = frameBuffer[3 + i * 4 + lineOffset];

//            uint16_t p0 = RED;
//            uint16_t p1 = BLACK;
//            uint16_t p2 = BLACK;
//            uint16_t p3 = BLACK;

            // Prepare 4 pixels for sending, which can be packed with 72 bits (aka 8 bytes)
            *(buffer + 0 + i * 9) = 1 << 7 | (p0 >> 9);
            *(buffer + 1 + i * 9) = 1 << 6 | ((p0 >> 2) & 0b111111) | (((p0 >> 8) & 1) << 7);
            *(buffer + 2 + i * 9) = 1 << 5 | ((p0 & 0b11) << 6) | ((p1 >> 11) & 0b11111);
            *(buffer + 3 + i * 9) = 1 << 4 | (((p1 >> 8) & 0b111) << 5) | ((p1 >> 4) & 0b1111);
            *(buffer + 4 + i * 9) = 1 << 3 | ((p1 & 0b1111) << 4) | ((p2 >> 13));
            *(buffer + 5 + i * 9) = 1 << 2 | (((p2 >> 8) & 0b11111) << 3) | ((p2 >> 6) & 0b11);
            *(buffer + 6 + i * 9) = 1 << 1 | (((p2 & 0b111111) << 2)) | (((p3 >> 15)));
            *(buffer + 7 + i * 9) = 1 << 0 | (((p3 >> 8) & 0x7f) << 1);
            *(buffer + 8 + i * 9) = p3 & 0xff;
        }
        ret = spi_device_transmit(spi8, &trans);
        assert(ret == ESP_OK);
    }

    free(buffer);

//    for (int i = 0; i < 9; i++) {
//        printf("buffer %d - %#01x\n", i, *(buffer + i));
//    }
}

void HSVtoRGB(uint8_t *red, uint8_t *green, uint8_t *blue, float h, float s, float v) {
    float fC = v * s; // Chroma
    float fHPrime = fmod(h / 60.0, 6);
    float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
    float fM = v - fC;

    float fR, fG, fB;

    if (0 <= fHPrime && fHPrime < 1) {
        fR = fC;
        fG = fX;
        fB = 0;
    } else if (1 <= fHPrime && fHPrime < 2) {
        fR = fX;
        fG = fC;
        fB = 0;
    } else if (2 <= fHPrime && fHPrime < 3) {
        fR = 0;
        fG = fC;
        fB = fX;
    } else if (3 <= fHPrime && fHPrime < 4) {
        fR = 0;
        fG = fX;
        fB = fC;
    } else if (4 <= fHPrime && fHPrime < 5) {
        fR = fX;
        fG = 0;
        fB = fC;
    } else if (5 <= fHPrime && fHPrime < 6) {
        fR = fC;
        fG = 0;
        fB = fX;
    } else {
        fR = 0;
        fG = 0;
        fB = 0;
    }

    fR += fM;
    fG += fM;
    fB += fM;

    *red = (uint8_t) (fR * 31);
    *green = (uint8_t) (fG * 63);
    *blue = (uint8_t) (fB * 31);
}

//Simple routine to generate some patterns and send them to the LCD. Don't expect anything too
//impressive. Because the SPI driver handles transactions in the background, we can calculate the next line
//while the previous one is being sent.
void display_pretty_colors() {

    int x, y;
    printf("Displaying pretty colors\n");

    if (INIT_ONLY) {
        while (1) { vTaskDelay(100 / portTICK_RATE_MS); };
    }

    int time = 0;
    int8_t direction = 4;
    while (1) {
        uint16_t color = 0;
        uint8_t r, g, b;
        HSVtoRGB(&r, &g, &b, time, 1, 1);

        for (y = 0; y < RESY; y++) {
            //Calculate a line.
            for (x = 0; x < RESX; x++) {
                uint16_t red = r << 11;
                uint16_t green = g << 5;
                uint16_t blue = b;
                frameBuffer[x + RESX * y] = red | green | blue;
            }
        }
        start_frame();
        send_frame();
        time += direction;

        if (time == 0 || time == 360)
            direction *= -1;

        //printf("%d, %d\n", time, direction);
    }
}

void app_main() {
    esp_err_t ret;

    //Initialize the SPI bus
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    assert(ret == ESP_OK);

    //Attach the LCD to the SPI bus
    ret = spi_bus_add_device(HSPI_HOST, &devcfg8bits, &spi8);
    assert(ret == ESP_OK);

    // 16 bits per pixel, hence the * 2
    frameBuffer = malloc(240 * 240 * 2);
    assert(frameBuffer != NULL);
    memset(frameBuffer, 0xff, 240 * 240 * 2);

    for (int y = 0; y < RESY; y++) {
        for (int x = 0; x < RESX; x++) {
            uint16_t red = 0 << 11;
            uint16_t green = 0 << 5;
            uint16_t blue = 0 << 0;
            frameBuffer[x + RESX * y] = red | green | blue;
        }
    }

    //Initialize the LCD
    screen_init();
    display_pretty_colors();
}