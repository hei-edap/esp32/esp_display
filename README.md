## Display with SPI master

This code displays a simple graphics with varying pixel colors on a 240x240 LCD
using a ST7789V display connected on an ESP-WROVER-KIT board.

Extra work was made to make the driver work with the 9 bit SPI mode used
on the display as other modes are not available.

This example has been superceded by the driver implementation `st7789v`
residing in [pmr-systems/esp32/esp_littlevgl/components/lvgl/drv] repository.

To adopt this example to another type of display or pinout, 
check [manin/spi_master_example_main.c] for comments 
that explain some of implementation details.
